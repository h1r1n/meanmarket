import { Injectable } from '@angular/core';
import {
  HttpClientModule,
  HttpClient,
  HttpResponse,
  HttpRequest
} from '@angular/common/http';
import 'rxjs/add/operator/map';
import * as decodeJWT from 'jwt-decode';
import { tokenNotExpired } from 'angular2-jwt';
import { Observable } from 'rxjs/Rx';

// Environments
import { environment } from '../../environments/environment';

@Injectable()
export class AuthService {
  result: any;
  cachedRequests: Array<HttpRequest<any>> = [];

  constructor(private _http: HttpClient) {}

  getUsers() {
    return this._http
      .get(environment.api_base + 'users')
      .map(result => (this.result = result));
  }

  // Get token from LocalStorage
  public getToken(): string {
      return localStorage.getItem('access-token');    
  }

  // Get token expiration time
  public getExpiration() {
    return decodeJWT(localStorage.getItem('access-token'));
  }

  // Check if the user is authenticated/user token is expired
  public isAuthenticated(): boolean {
    return !this.isExpired();
  }

  // Check if the user token exists and is expired
  public isExpired(): boolean {

    let isExpired:boolean = true;

    // If token exists -> get the token expire time
    try{
      const tokenPayload = decodeJWT(this.getToken());
      const current_time = new Date().getTime();
      if (current_time > tokenPayload.expires) {
        isExpired = true;
      } else {
        isExpired = false;
      }
    } finally {
      return isExpired;
    }
  }

  public collectFailedRequest(request): void {
    this.cachedRequests.push(request);
  }

  public retryFailedRequests(): void {
    // retry the requests. this method can be called after the token is refreshed
  }

  // Get User from LocalStorage
  public getActiveUser(){
    return JSON.parse(localStorage.getItem('user'));
  }


  // API Auth Functions

  signIn(loginForm) {
    return this._http
      .post(environment.api_base + 'signin', loginForm)
      .map(result => (this.result = result));
  }

  signUp(registerForm) {
    return this._http
      .post(environment.api_base + 'signup', registerForm)
      .map(result => (this.result = result));
  }

  signOut() {
    localStorage.removeItem('access-token');
    localStorage.removeItem('user');
  }
}
