import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Rx';

//Services
import { AuthService } from '../services/auth.service';

@Injectable()
export class NoAuthGuard implements CanActivate {
  
  public canAccess;
  
  constructor(public _authService: AuthService) {}

    canActivate(
     route: ActivatedRouteSnapshot,
     state: RouterStateSnapshot
  ): boolean {
        const isAuth = this._authService.isAuthenticated();
        return isAuth;
    }
}