import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';

// Services
import { AuthService } from '../../services/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  loginForm;
  error: String;

  constructor(private _router: Router, private _authService: AuthService) { }

  ngOnInit() {

    this.loginForm = new FormGroup({
      username: new FormControl('', [Validators.required, Validators.minLength(5), Validators.maxLength(20)] ),
      password: new FormControl('', Validators.required)
    });

  }

  // Try to login
  /*
    IR: On Successful Sign in => put Token adn User in Local Storage
  */
  onLogin() {
    this._authService.signIn(this.loginForm.value)
    .subscribe(
      (res) => {
        localStorage.setItem( 'access-token' , res['token'] );
        localStorage.setItem( 'user' , JSON.stringify(res['user']) );
        this._router.navigate(['/myaccount']);
      },
      (err) => { this.error = err.error.error; }
    );
  }

}
