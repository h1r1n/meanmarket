import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';
// Services
import { AuthService } from '../../services/auth.service';


@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {

  registerForm;
  error: String;

  constructor(private _router: Router, private _authService: AuthService) { }

  ngOnInit() {

    this.registerForm = new FormGroup({
      username: new FormControl('', [Validators.required, Validators.minLength(4), Validators.maxLength(20)] ),
      displayName: new FormControl('', [Validators.required, Validators.minLength(4), Validators.maxLength(20)] ),
      email: new FormControl('', [Validators.required, Validators.email] ),
      password: new FormControl('', Validators.required)
    });

  }

  // Try to register
  onRegister() {
    this._authService.signUp(this.registerForm.value)
    .subscribe(
      (res) => {
        this._router.navigate(['/login']);
      },
      (err) => { this.error = err.error._message; }
    );
  }

}
