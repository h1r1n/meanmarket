import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

// Modules
import { Http, RequestOptions, HttpModule } from '@angular/http';
import { HTTP_INTERCEPTORS, HttpBackend , HttpClientModule, HttpClient } from '@angular/common/http';
import { AuthHttp, AuthConfig } from 'angular2-jwt';
import { RouterModule } from '@angular/router';
import { ReactiveFormsModule } from '@angular/forms';

// Services
import { AuthService } from './services/auth.service';
import { NoAuthGuard } from './guards/no-auth-guard.service';
import { TokenInterceptorService } from './interceptors/token-interceptor.service';
import { JwtInterceptorService } from './interceptors/jwt-interceptor.service';

// Components
import { AppComponent } from './app.component';
import { LoginComponent } from './auth/login/login.component';
import { RegisterComponent } from './auth/register/register.component';
import { MyaccountComponent } from './profile/myaccount/myaccount.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    RegisterComponent,
    MyaccountComponent
  ],
  imports: [
    BrowserModule,
    ReactiveFormsModule,
    HttpClientModule,
    RouterModule.forRoot([
      // Auth routes
      {
        path: 'login',
        component: LoginComponent
      },
      {
        path: 'register',
        component: RegisterComponent
      },
      // Profile Routes
      {
        path: 'myaccount',
        component: MyaccountComponent,
        canActivate: [NoAuthGuard]
      },
    ])
  ],
  providers: [
    AuthService,
    NoAuthGuard,
    { provide: HTTP_INTERCEPTORS, useClass: TokenInterceptorService, multi: true },
    { provide: HTTP_INTERCEPTORS, useClass: JwtInterceptorService, multi: true }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
