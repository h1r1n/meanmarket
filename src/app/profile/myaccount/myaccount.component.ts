import { Component, OnInit } from '@angular/core';

// Services
import { AuthService } from '../../services/auth.service';

@Component({
  selector: 'app-myaccount',
  templateUrl: './myaccount.component.html',
  styleUrls: ['./myaccount.component.scss']
})
export class MyaccountComponent implements OnInit {

  activeUser;

  constructor(public _authService: AuthService ) { 
    this.activeUser = _authService.getActiveUser();
  }

  ngOnInit() {
  }

  public checkToken() {
    console.log(this._authService.isExpired())
    // console.log(localStorage);
    console.log('IS AUTH:' + this._authService.isAuthenticated());
  }

}
