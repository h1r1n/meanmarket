import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor,
  HttpErrorResponse,
  HttpResponse
} from '@angular/common/http';
import { AuthService } from '../services/auth.service';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class TokenInterceptorService implements HttpInterceptor {
  constructor( public _router: Router, public _authService: AuthService ) {}

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

    if(this._authService.isExpired()){
      this._authService.signOut();
      this._router.navigate(['/login']);
    }

    request = request.clone({
      setHeaders: {
        'access-token': `${this._authService.getToken()}`
      }
    });

    return next.handle(request);

  }

}
